package Ex2;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class GestionnaireRdv {
	
    private List<RendezVous>rendezVousList;

    public GestionnaireRdv() {
        this.rendezVousList = new ArrayList<>();
    }
    
    //veriff si les patients existent, si l'id est supp de 0 alors valide
    private boolean patientValide(int patientId) {
        return patientId > 0;
    }
    //verrif si les medecins existent, si l'id est supp de 0 alors valide
    private boolean medecinValide(int medecinId) {
        return medecinId > 0;
    }
    //verrif si le medecin est dispo, tout le temps true
    private boolean medecinDisponible(int medecinId, LocalDateTime dateTime) {
        return true;
    }
    //verrif si patient est occupe
    private boolean patientOccupe(int patientId, LocalDateTime dateTime) {
        return false;
    }
    //verrif si le medecin est occupe
    private boolean medecinOccupe(int medecinId, LocalDateTime dateTime) {
        return false;
    }
    //reservation de rdv
    public boolean reserverRdv(int patientId, int medecinId, LocalDateTime dateTime) {
        if (!patientValide(patientId) || !medecinValide(medecinId)) {
            return false;
        }

        if (!medecinDisponible(medecinId, dateTime)) {
            return false;
        }

        if (patientOccupe(patientId, dateTime)) {
            return false;
        }

        if (medecinOccupe(medecinId, dateTime)) {
            return false;
        }

        RendezVous rendezVous = new RendezVous(patientId, medecinId, dateTime);
        rendezVousList.add(rendezVous);
        return true;
    }

}
