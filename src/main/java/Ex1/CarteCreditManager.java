package Ex1;

public class CarteCreditManager {
    public boolean verifCarte(String numCarte, String cvv) {
        boolean carteValide = CarteNumOk(numCarte) && CvvOk(cvv);//verrif si 16 caractères pour cvv et 3 caractères pour cvv
        if (carteValide) {
            System.out.println("informations OK");//affiche que la carte est ok
        } else {
            System.out.println("informations pas OK");//affiche que ka carte est pas valide
        }
        return carteValide;
    }
    private boolean CvvOk(String cvv) {
        return cvv.length() == 3;//on a besoin que la carte est 3 caractères de sécu cvv
    }
    private boolean CarteNumOk(String numCarte) {
        return numCarte.length() == 16;//on a besoin que la carte est 16 caractères
    }
}
