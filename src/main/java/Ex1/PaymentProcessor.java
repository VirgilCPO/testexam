package Ex1;

public class PaymentProcessor {
    
    public boolean payement(double montant, String numCarte, String cvv) {
        CarteCreditManager carteCreditManager = new CarteCreditManager();
        boolean carteValide = carteCreditManager.verifCarte(numCarte, cvv);
        if (carteValide) {
            PaymentGateway paymentGateway = new PaymentGateway();
            String idTransaction = paymentGateway.payement(montant);
            if (idTransaction != null) {
                return true;
            }
        }
        return false;
    }
}
