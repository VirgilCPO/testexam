package Ex2;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;

public class ReservationTest {
	//rappel on a mis si l'id est > 0 l'id est ok
    @Test
    public void testReservationValide() {
        GestionnaireRdv gestionnaireRdv = new GestionnaireRdv();
        int patientId = 1;
        int medecinId = 1;
        LocalDateTime dateTime = LocalDateTime.of(2022, 4, 1, 10, 0, 0);

        boolean reservationEffectuee = gestionnaireRdv.reserverRdv(patientId, medecinId, dateTime);

        Assert.assertTrue(reservationEffectuee);
    }

    @Test
    public void testReservationPatientInvalide() {
        GestionnaireRdv gestionnaireRdv = new GestionnaireRdv();
        int patientId = 0;
        int medecinId = 1;
        LocalDateTime dateTime = LocalDateTime.of(2022, 4, 1, 10, 0, 0);

        boolean reservationEffectuee = gestionnaireRdv.reserverRdv(patientId, medecinId, dateTime);

        Assert.assertFalse(reservationEffectuee);
    }

    @Test
    public void testReservationMedecinInvalide() {
        GestionnaireRdv gestionnaireRdv = new GestionnaireRdv();
        int patientId = 1;
        int medecinId = 0;
        LocalDateTime dateTime = LocalDateTime.of(2022, 4, 1, 10, 0, 0);

        boolean reservationEffectuee = gestionnaireRdv.reserverRdv(patientId, medecinId, dateTime);

        Assert.assertFalse(reservationEffectuee);
    }

    @Test
    public void testReservationDateIndisponible() {
        GestionnaireRdv gestionnaireRdv = new GestionnaireRdv();
        int patientId = 1;
        int medecinId = 1;
        LocalDateTime dateTime = LocalDateTime.of(2022, 4, 1, 10, 0, 0);

        boolean medecinDisponible = false;//pas dispo

        boolean reservationEffectuee = gestionnaireRdv.reserverRdv(patientId, medecinId, dateTime);

        Assert.assertFalse(reservationEffectuee);
    }

    @Test
    public void testReservationPatientOccupe() {
        GestionnaireRdv gestionnaireRdv = new GestionnaireRdv();
        int patientId = 1;
        int medecinId = 1;
        LocalDateTime dateTime = LocalDateTime.of(2022, 4, 1, 10, 0, 0);

        boolean patientOccupe = true;//pas dispo

        boolean reservationEffectuee = gestionnaireRdv.reserverRdv(patientId, medecinId, dateTime);

        Assert.assertFalse(reservationEffectuee);
    }

    @Test
    public void testReservationMedecinOccupe() {
        GestionnaireRdv gestionnaireRdv = new GestionnaireRdv();
        int patientId = 1;
        int medecinId = 1;
        LocalDateTime dateTime = LocalDateTime.of(2022, 4, 1, 10, 0, 0);

        boolean medecinOccupe = true;//pas dispo

        boolean reservationEffectuee = gestionnaireRdv.reserverRdv(patientId, medecinId, dateTime);

        Assert.assertFalse(reservationEffectuee);
    }

    //ici le tableau de reservation est pas fait donc ca marche pas
    @Test
    public void testReservationParametre() {
        GestionnaireRdv gestionnaireRdv = new GestionnaireRdv();
        
        int patientId1 = 1;
        int medecinId1 = 1;
        LocalDateTime dateTime1 = LocalDateTime.of(2022, 4, 1, 10, 0, 0);
        boolean reservationEffectuee1 = gestionnaireRdv.reserverRdv(patientId1, medecinId1, dateTime1);
        Assert.assertTrue(reservationEffectuee1);
        
        int patientId2 = 2;
        int medecinId2 = 1;
        LocalDateTime dateTime2 = LocalDateTime.of(2022, 4, 1, 11, 0, 0);
        boolean reservationEffectuee2 = gestionnaireRdv.reserverRdv(patientId2, medecinId2, dateTime2);
        Assert.assertFalse(reservationEffectuee2);
        
        int patientId3 = 1;
        int medecinId3 = 2;
        LocalDateTime dateTime3 = LocalDateTime.of(2022, 4, 2, 13, 0, 0);
        boolean reservationEffectuee3 = gestionnaireRdv.reserverRdv(patientId3, medecinId3, dateTime3);
        Assert.assertTrue(reservationEffectuee3);
        
        int patientId4 = 2;
        int medecinId4 = 2;
        LocalDateTime dateTime4 = LocalDateTime.of(2022, 4, 2, 14, 0, 0);
        boolean reservationEffectuee4 = gestionnaireRdv.reserverRdv(patientId4, medecinId4, dateTime4);
        Assert.assertTrue(reservationEffectuee4);
        
        int patientId5 = 3;
        int medecinId5 = 2;
        LocalDateTime dateTime5 = LocalDateTime.of(2022, 4, 2, 15, 0, 0);
        boolean reservationEffectuee5 = gestionnaireRdv.reserverRdv(patientId5, medecinId5, dateTime5);
        Assert.assertTrue(reservationEffectuee5);
        
        int patientId6 = 4;
        int medecinId6 = 2;
        LocalDateTime dateTime6 = LocalDateTime.of(2022, 4, 2, 16, 0, 0);
        boolean reservationEffectuee6 = gestionnaireRdv.reserverRdv(patientId6, medecinId6, dateTime6);
        Assert.assertFalse(reservationEffectuee6);
    }
}
