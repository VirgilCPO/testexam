package Ex1;

import org.junit.*;
import static org.junit.Assert.*;

public class PaymentIntegrationTest {

    private CarteCreditManager CarteCreditManager;
    private PaymentGateway paymentGateway;
    private PaymentProcessor paymentProcessor;

    @Before
    public void init() {
        CarteCreditManager = new CarteCreditManager();
        paymentGateway = new PaymentGateway();
        paymentProcessor = new PaymentProcessor();
    }

    @Test
    public void payementValide() {
        String numCarte = "9999999999999999";
        String cvv = "123";
        double montant = 9.0;

        boolean carteOK = CarteCreditManager.verifCarte(numCarte, cvv);
        assertTrue("cate non OK", carteOK);

        String idTransaction = paymentGateway.payement(montant);
        assertNotNull("id mauvais", idTransaction);

        boolean paymentProcessed = paymentProcessor.payement(montant, numCarte, cvv);
        assertTrue("paiement pas ok", paymentProcessed);
    }

    @Test
    public void payementNonValide() {
        String numCarte = "123456789012345094034903940394203940182";
        String cvv = "999";
        double montant = 99999999999999.0;

        boolean cardVerified = CarteCreditManager.verifCarte(numCarte, cvv);
        assertFalse("La vérification de la carte de crédit a réussi", cardVerified);

        boolean paymentProcessed = paymentProcessor.payement(montant, numCarte, cvv);
        assertFalse("payement non ok", paymentProcessed);
    }

}
